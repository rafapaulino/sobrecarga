﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static double media;
        static string metodoSobrecarga;
        static void Main(string[] args)
        {
            calcular(5, 8);
            calcular("5","8");
            mostrarMedia();
        }

        public static void calcular(double nota1, double nota2)
        {
            media = (nota1 + nota2) / 2;
            metodoSobrecarga = "calcular(double nota1, double nota2)";
        }

        public static void calcular(string nota1,string nota2)
        {
            media = (double.Parse(nota1) + double.Parse(nota2)) / 2;
            metodoSobrecarga = "calcular(string nota1, string nota2)";
        }

        public static void mostrarMedia()
        {
            Console.WriteLine("A média é " + media);
            Console.WriteLine("Método chamado " + metodoSobrecarga);
            Console.ReadKey();
        }
    }
}
